import {UserModule} from 'user';
import {NgModule} from '@angular/core';

@NgModule({
  imports: [UserModule]
})
export class UserWrapperModule {}
